TARGET_FOLDER="/home/hubert/public_html/ctfhc-drama-2012"

clean:
	find . -name "*~" -delete

mkdir:
	mkdir -p $(TARGET_FOLDER)

deploy: mkdir
	git archive master | tar -x -C $(TARGET_FOLDER) -f -

test: mkdir
	cp -fr * $(TARGET_FOLDER)
